# List of users made in ReactJS and Material-UI #

Just a simple list of users, which is displayed from on our JSON file.
After click on a specific user it routes us to detailed view. 
Data flow is with React routing: It should not be used to often and with bigger data, but at the moment it was the only solution I can provide for this.

Link: [My Domain](http://vibs-glasses.com/users/dist/)

Language: Polish