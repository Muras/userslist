// Styles used directly in React for in-line styling

const styles = {
  mainPageSize: {
    width: '80vw',
    maxWidth: '1200px',
    minWidth: '800px',
    padding: '5px',
    overflow: 'hidden',
    minHeight: '500px'
  },
  tableRow: {
    fontSize: '16px !important'
  },
  idColumn: {
    width: '20px'
  }
}
  
export default styles