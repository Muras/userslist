import React from 'react'

//Additional functions used in project
// displayUserName = > defines what title should be rendered

const helper = {
  displayUserName(props) {
    let header = props.input.route.header
    let userName = props.input.location.query.names + ' ' + props.input.location.query.surname

    if (props.input.location.query.names !== undefined) {
      return <span>{header}: {userName}</span>
    } else {
      return <span>{header}</span>
    }
  },
  scrollToPageTop () {
    return window.scrollTo(0, 0)
  }
}


export default helper
export const scrollToPageTop = helper.scrollToPageTop