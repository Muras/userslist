import React from 'react'
//Logic and states for single user row
import UsersTableRow from '../components/users_table/UsersTableRow'


const UsersTableRowContainer = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },
  getInitialState() {
    return {
      userData: []
    }
  },
  handleUserClick(e) {
    this.setState({ userData: this.props.users },
      function () {
        this.context.router.push({
          pathname: '/user',
          query: this.props.users
        })
      }
    )
  },
  render() {
    return (
      <UsersTableRow
        id={this.props.users.id}
        names={this.props.users.names}
        surname={this.props.users.surname}
        birth_date={this.props.users.birth_date}
        pesel={this.props.users.pesel}
        onUserClick={this.handleUserClick} />
    )
  }
})

export default UsersTableRowContainer