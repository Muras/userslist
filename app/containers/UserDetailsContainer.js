import React from 'react'

// Passing values from router to user_details component

import UserDetails from '../components/UserDetails'

class UserDetailsContainer extends React.Component {

  render() {
    let query = this.props.location.query
    return (
      <UserDetails 
        name={query.names}
        surname={query.surname}
        secondName={query.second_name}
        familyName={query.family_name}
        pesel={query.pesel}
        nip={query.nip}
        phone={query.phone}
        mail={query.mail}
        street={query.street}
        streetNumber={query.house_number} 
        doorsNumber={query.doors_number}
        city={query.city}
        postalCode={query.postal_code}
        idNumber={query.ID_number}
        idType={query.identification}
        releasedBy={query.released_by}
        citizenship={query.citizenship}
        nationality={query.nationality}
        mothersName={query.mothers_name}
        fathersName={query.fathers_name}
        mothersSurname={query.mothers_surname}
        gender={query.gender} 
        education={query.education}
        birthDate={query.birth_date}
        birthPlace={query.birth_place} />
    )
  }
}

export default UserDetailsContainer