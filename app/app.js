import React from 'react'
import ReactDOM from 'react-dom'

import routes from './routes'
import styles from './styles/main.css'

ReactDOM.render(
  routes, document.getElementById('app')
)