import React from 'react'
//Main component which is present on both sites

//==================== Material UI Elements ======================//
import Paper from 'material-ui/Paper'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import IconActionHome from 'material-ui/svg-icons/action/home'
import {Link } from 'react-router'
import {white} from 'material-ui/styles/colors'

import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()
//================================================================//

//===================== Additional utilities =====================//
import styles from '../styles/styles'
import helpFunctions from '../utils/helpFunctions'
const DisplayUserName = helpFunctions.displayUserName
//================================================================//


const Main = React.createClass({
  render() {
    return (
      <MuiThemeProvider>
        <Paper style={styles.mainPageSize} zDepth={1}>
          <AppBar
            iconElementLeft={<Link to='/'><IconButton><IconActionHome color={white}/></IconButton></Link>}
            title={ <DisplayUserName input={this.props.children.props}/> } />
          {this.props.children}
        </Paper>
      </MuiThemeProvider>
    )
  }
})

module.exports = Main