import React from 'react'

import { TableRow, TableRowColumn} from 'material-ui/Table'
// Layout of a single user in our table

//I had to create new Row class beacause currenty there is a bug in Material-UI
//where onClick events do not work directly on table elements... :(
import styles from '../../styles/styles'

class Row extends React.Component {
  render() {
    const { id, names, surname, birth_date, pesel } = this.props
    return (
      <TableRow  onClick={this.props.onClickEvent} > 
        <TableRowColumn style={styles.idColumn}>{id}</TableRowColumn>
        <TableRowColumn>{names}</TableRowColumn>
        <TableRowColumn>{surname}</TableRowColumn>
        <TableRowColumn>{birth_date}</TableRowColumn>
        <TableRowColumn>{pesel}</TableRowColumn>
      </TableRow>
    )
  }
}

function UsersTableRow(props) {
  return (
    <Row
      id={props.id}
      names={props.names}
      surname={props.surname}
      birth_date={props.birth_date}
      pesel={props.pesel}
      onClickEvent={props.onUserClick}/> 
  )
}

export default UsersTableRow