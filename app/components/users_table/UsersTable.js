import React from 'react'
//Creating view of a UsersTable, TableRow + TableHeader
// Putting all elements together: this component is rendered 

import UsersTableRowContainer from '../../containers/UsersTableRowContainer'
import UsersTableHeader from './UsersTableHeader' 

function UsersTable (props) {
  return (
    <UsersTableHeader>
      {
        props.route.users.map((user, index) => (
          <UsersTableRowContainer
            users={user}
            key={index}
          />
        ))
      }
    </UsersTableHeader>
  )
}

export default UsersTable