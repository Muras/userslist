import React from 'react'
//Here we have Layout of table header

//================== Material-UI Elements=================//
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
//========================================================//
import styles from '../../styles/styles'

const UsersTableHeader = React.createClass({
  render() {
    return (
      <Table onCellClick={this.props.onUserClick}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn style={styles.idColumn}>ID</TableHeaderColumn>
            <TableHeaderColumn>Imie</TableHeaderColumn>
            <TableHeaderColumn>Nazwisko</TableHeaderColumn>
            <TableHeaderColumn>Data urodzenia</TableHeaderColumn>
            <TableHeaderColumn>PESEL</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false} showRowHover={true} stripedRows={true}>
          {this.props.children}
        </TableBody>
      </Table>
    )
  }
})

export default UsersTableHeader