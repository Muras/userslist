import React from 'react'

// Layout of user_details site. 

//====================Material UI Elements=================//
import Divider from 'material-ui/Divider'
import Paper from 'material-ui/Paper'
import {List, ListItem} from 'material-ui/List'
//===========================================================//
import {IconAddress, IconFace, IconInfo, IconContact, IconId } from './Icons'

function UserDetails(props) {
  return (
    <div className='user_details--wrapper'>
      <div className='user_details--horizontalSeparator'>
        <List className='user_details--box'>
          <h3 className='user_details--box-title'>
            <span className='user_details--divider'> <IconFace /> Dane osobowe</span>
          </h3>
          <ListItem className='user_details--box-singleValue' primaryText={props.name} secondaryText='Imie'/>
          <ListItem className='user_details--box-singleValue secondaryValue' primaryText={props.secondName} secondaryText='Drugie imie'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.surname} secondaryText='Nazwisko'/>
          <ListItem className='user_details--box-singleValue secondaryValue' primaryText={props.familyName} secondaryText='Nazwisko rodowe'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.birthDate} secondaryText='Data urodzenia'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.birthPlace} secondaryText='Miejsce urodzenia'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.pesel} secondaryText='PESEL'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.nip} secondaryText='NIP'/>
        </List>
        <List className='user_details--box'>
          <h3 className='user_details--box-title'>
            <span className='user_details--divider'><IconAddress/> Adres</span>
          </h3>
          <ListItem className='user_details--box-singleValue-oneLine' primaryText={props.street} secondaryText='Ulica'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.streetNumber} secondaryText='Numer domu'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.doorsNumber} secondaryText='Numer mieszkania'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.city} secondaryText='Miasto'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.postalCode} secondaryText='Kod pocztowy'/>
        </List>
      </div>
      <div className='user_details--horizontalSeparator'>
        <List className='user_details--box'>
          <h3 className='user_details--box-title'>
            <span className='user_details--divider'><IconId /> Dokument tożsamości</span>
          </h3>
          <ListItem className='user_details--box-singleValue' primaryText={props.idType} secondaryText='Rodzaj dokumentu'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.idNumber} secondaryText='Numer i seria dokumentu'/>
          <ListItem className='user_details--box-singleValue-oneLine' primaryText={props.releasedBy} secondaryText='Wydany przez'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.citizenship} secondaryText='Obywatelstwo'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.nationality} secondaryText='Narodowość'/>
        </List>
        <List className='user_details--box'>
          <h3 className='user_details--box-title'>
            <span className='user_details--divider'><IconContact /> Kontakt</span>
          </h3>
          <ListItem className='user_details--box-singleValue-oneLine' primaryText={props.phone} secondaryText='Telefon'/>
          <ListItem className='user_details--box-singleValue-oneLine' primaryText={props.mail} secondaryText='e-mail' />
        </List>
      </div>
      <div className='user_details--horizontalSeparator'>
        <List className='user_details--box'>
          <h3 className='user_details--box-title'>
            <span className='user_details--divider'><IconInfo /> Dodatkowe</span>
          </h3>
          <ListItem className='user_details--box-singleValue' primaryText={props.gender} secondaryText='Płeć'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.fathersName} secondaryText='Imie ojca'/>
          <ListItem className='user_details--box-singleValue' primaryText={props.education} secondaryText='Wykształcenie'/>          
          <ListItem className='user_details--box-singleValue' primaryText={props.mothersName + ' ' + props.mothersSurname} secondaryText='Imie i nazwisko matki'/>
        </List>
      </div>
    </div>
  )
}

export default UserDetails