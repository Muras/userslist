import React from 'react'
import { Route, Router, hashHistory, IndexRoute } from 'react-router'

import UsersTable from './components/users_table/UsersTable'
import UserDetailsContainer from './containers/UserDetailsContainer'
import Main from './components/Main'
import users from './database/users_list.json'
import {scrollToPageTop} from './utils/helpFunctions' 

const routes = (
  <Router onUpdate={scrollToPageTop}  history={hashHistory} >
    <Route path='/' component={Main} >
      <IndexRoute component={UsersTable} users={users} header='Lista osób'/>
      <Route path='/user' component={UserDetailsContainer} header='Dane szczegółowe'/>
    </Route>
  </Router>
);

export default routes